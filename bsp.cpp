#include <iostream>
#include "mcbsp.hpp"

using namespace std;

class HelloWorld : public mcbsp::BSP_program
{
    protected:
        virtual void spmd(void);
        virtual BSP_program * newInstance();
    public:
        HelloWorld();
};

void HelloWorld::spmd()
{
    cout << "Hello World from thread " << bsp_pid() << "\n";
}

mcbsp::BSP_program * HelloWorld::newInstance()
{
    return new HelloWorld();
}

HelloWorld::HelloWorld(){}


int main(int argc, char **argv)
{
    HelloWorld p;

    p.begin();

    return 0;
}
