MCBSP_DIR= /ufs/makarim/bsp/MulticoreBSP-for-C
INCLUDE_DIR= ${MCBSP_DIR}/include
LIB_DIR= ${MCBSP_DIR}/lib

CPP= g++
OPTFLAGS= -O3 -funroll-loops -march=native
CPPFLAGS= -std=c++11 -Wall ${OPTFLAGS} -I. -I${INCLUDE_DIR}
LFLAGS= ${LIB_DIR}/libmcbsp1.2.0.a -lm `${MCBSP_DIR}/deplibs.sh gcc -ansi -std=c99`

OBJS= bsp.opp

all: bsp

bsp: ${OBJS}
	${CPP} ${CPPFLAGS} -o $@ ${OBJS} ${LFLAGS}

%.opp: %.cpp %.hpp
	${CPP} ${CPPFLAGS} -c -o $@ $(^:%.hpp=)

%.opp: %.cpp
	${CPP} ${CPPFLAGS} -c -o $@ $^

clean:
	rm -f bsp *.opp

