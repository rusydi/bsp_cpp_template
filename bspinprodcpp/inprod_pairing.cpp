#include <cstdio>
#include <vector>
#include "mcbsp.hpp"

using namespace std;

signed q; //we will use P = 2^q processors
unsigned P; //number of processors

class InProd : public mcbsp::BSP_program
{
    protected:
        virtual void spmd(void);
        double bspip_pairing(int , int , int , const vector<double> & , const vector<double> & );
        int nloc(int, int, int);
        virtual BSP_program * newInstance()
        {
            return new InProd();
        }

    public:
        InProd(){};
};

int InProd::nloc(int p, int s, int n)
{
    return (n+p-s-1)/p;
}

double InProd::bspip_pairing(int p, int s, int n, const vector<double> & x, const vector<double> & y)
{
    double alpha_s, alpha_t;
    int t;

    bsp_push_reg(&alpha_t, sizeof(double));
    bsp_sync();

    alpha_s = 0.0;
    for(int i=0; i < nloc(p, s, n); ++i)
    {
        alpha_s += x[i] * y[i];
    }
    
    bsp_sync();

    for(int k=0; k < q; ++k)
    {
        //if s mod 2^{k+1} < 2^k
        if ((s % (1 << (k+1))) < (1 << k))
            t = (s + (1 << k)) % p;
        else
            t = (s - (1 << k)) % p;

        bsp_put(t, &alpha_s, &alpha_t, 0, sizeof(double));

        bsp_sync();

        alpha_s += alpha_t;
    }

    bsp_pop_reg(&alpha_t);

    return alpha_s;
}

void InProd::spmd()
{
    vector<double> x;
    double alpha, time0, time1;
    signed n, nl, iglob;
    unsigned p, s;

    p = bsp_nprocs();
    s = bsp_pid();

    if (s == 0)
    {
        printf("Please enter n:\n"); fflush(stdout);
        scanf("%d", &n);
        if (n < 0)
            bsp_abort("Error in input: n is negative\n");
    }

    bsp_push_reg(&n, sizeof(signed));
    bsp_sync();

    bsp_get(0, &n, 0, &n, sizeof(n));
    bsp_sync();
    bsp_pop_reg(&n);

    nl = nloc(p, s, n);
    x.reserve(nl);
    for(int i=0; i < nl; ++i)
    {
        iglob = i*p + s;
        x[i] = iglob + 1;
    }
    bsp_sync();
    
    time0 = bsp_time();
    alpha = bspip_pairing(p, s, n, x, x);
    bsp_sync();
    time1 = bsp_time();

    printf("Processor %d: sum of squares up to %d*%d is %.lf\n",
            s, n, n, alpha); fflush(stdout);

    if (s == 0)
    {
        printf("This took only %.6lf seconds.\n", time1-time0);
        fflush(stdout);
    }
}

int main(int argc, char **argv)
{
    printf("Enter q (we will use 2^q processors) : "); fflush(stdout);
    scanf("%d",&q);
    P = (1 << q);
    if (P > bsp_nprocs()){
        printf("Sorry, not enough processors available.\n"); fflush(stdout);
        return 1;
    }

    InProd inprod;
    inprod.begin(P);
    return 0;
}
