#ifndef SPARSE_VECTOR_HPP
#define SPARSE_VECTOR_HPP

#include<iostream>
#include<vector>

template<class T, std::size_t N>
class sparse_vector
{
    private:
        std::vector<T> _index, _val;
    public:
        static std::vector<int> loc;

        sparse_vector(const std::vector<T> & v)
        {
                
            _index.reserve(v.size());
            _val.reserve(v.size());

            for(unsigned i=0; i < v.size(); ++i)
            {
                if (v[i] != 0)
                {
                    _index.emplace_back(i);
                    _val.emplace_back(v[i]);
                }
            }

            _index.shrink_to_fit();
            _val.shrink_to_fit();
        }

        unsigned no_of_nonzero() const
        {
            return _val.size();
        }
       
        T index(int i) const
        {
            return _index[i];
        }

        T value(int i) const
        {
            return _val[i];
        }
        
        sparse_vector& operator+=(const sparse_vector & y)
        {
            for(unsigned j=0; j < y.no_of_nonzero(); ++j)
                loc[ y.index(j) ] = j;

            for(unsigned j=0; j < this->no_of_nonzero(); ++j)
            {
                unsigned i = _index[j];

                if (sparse_vector::loc[i] != -1)
                {
                    _val[j] += y.value( sparse_vector::loc[i] );
                    sparse_vector::loc[i] = -1;
                }
            }

            for(unsigned j=0; j < y.no_of_nonzero(); ++j)
            {
                unsigned i = y.index(j);

                if (sparse_vector::loc[i] != -1)
                {
                    _index.emplace_back(i);
                    _val.emplace_back( y.value(j) );
                    sparse_vector::loc[i] = -1;
                }
            }

            return *this;
        }
        
};

template<class T, std::size_t N>
std::vector<int> sparse_vector<T, N>::loc(N, -1);

template<class T, std::size_t N>
std::ostream& operator<<(std::ostream& os, const sparse_vector<T, N> & v)
{
    for(unsigned j=0; j < v.no_of_nonzero(); ++j)
    {
        sparse_vector<T, N>::loc[ v.index(j) ] = j;
    }

    os << "(";
    for(unsigned j=0; j < N; ++j)
    {
        if (sparse_vector<T, N>::loc[j] == -1)
            os << "0";
        else
        {
            T n = v.value( sparse_vector<T, N>::loc[j] );
            os << n;
            sparse_vector<T, N>::loc[j] = -1;
        }
        if (j != N-1) os << ", ";
    }
    os << ")";

    return os;
}

#endif
